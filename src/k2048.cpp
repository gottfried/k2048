/***************************************************************************
 *   Copyright (C) %{CURRENT_YEAR} by %{AUTHOR} <%{EMAIL}>                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#include "k2048.h"

#include <QDeclarativeView>
#include <QDeclarativeContext>

#include <kdeclarative.h>
#include <KAboutApplicationDialog>
#include <KStandardDirs>
#include <kdebug.h>
#include <QPointer>
#include <kcomponentdata.h>

K2048::K2048()
    : KMainWindow(),
    m_view(new QDeclarativeView(this))
{
    setCentralWidget(m_view);

    KDeclarative kDeclarative;
    kDeclarative.setDeclarativeEngine(m_view->engine());
    kDeclarative.initialize();
    kDeclarative.setupBindings();

    m_view->setStyleSheet("background-color: transparent;");
    m_view->rootContext()->setContextProperty("context", this);
    m_view->setResizeMode(QDeclarativeView::SizeRootObjectToView);
    m_view->setSource(QUrl::fromLocalFile(KGlobal::dirs()->findResource("appdata", "qml/main.qml")));
    m_view->setMinimumSize(QSize(450, 495));
}

K2048::~K2048()
{
}

void K2048::about()
{
    QPointer<KAboutApplicationDialog> aboutDialog = new KAboutApplicationDialog(KGlobal::mainComponent().aboutData(), this);
    aboutDialog->exec();
    delete aboutDialog;
}


#include "k2048.moc"
